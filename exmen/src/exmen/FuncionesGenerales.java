/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/
package exmen;


import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import javax.swing.JTable;
import org.jdesktop.swingx.JXTable;


/**
 *Esta clase cuenta con una serie de metodos que podran ser llamados desde cualquier
 * clase porque van a ser static.
 * y como son muy utilizados en varias clases es mejor tenerlos aqui
 * en un punto en comun.
 * por ejemplo la hora y la fecha.
 *
 * @author Administrador
 */

public class FuncionesGenerales {
    
    public static final int Arriba_derecha=1;
    public static final int Arriba_izquierda=2;
    public static final int Abajo_derecha=3;
    public static final int Abajo_izquierda=4;
    public static final int  Centro_debajo=1;
    /**
     *@deprecated Now used Fecha(long mysesion,Logs mylogs)  o usar
     * ava.sql.Date Fecha(long mysesion,Logs mylogs,boolean sql) dependiendo de las necesidades.
     * Otra cosa si se desea un String no una Date lo que se debe hacer es usar el metodo.
     * FormatearFecha(null, Centro_debajo)
     * @return la fecha que tiene el sistema.
     */
    public static String Fecha(){
        Calendar c=Calendar.getInstance();
        String fecha=""+c.get(c.YEAR)+"-"+(c.get(c.MONTH)+1)+"-"+c.get(c.DAY_OF_MONTH);
        return fecha;
    }
     /**
     * Este metodo busca en la BD la carpteta donde estan guardados los reportes de
     * Jasper
     * @param mylogs
     * @param mysesion
     * @return un String con la carpeta en question.
     */
    public static String Carpeta_de_reportes_Jasper(Logs mylogs,long mysesion){
        String SQL =
                "SELECT valor"+"\r\n"+
                "  FROM configuraciones.configuracionesespeciales"+"\r\n"+
                " WHERE id = 6";
        Object data[][]=OperacionesBD.Select(SQL, mylogs, mysesion);
        if(data.length==0 || data[0][0]==null)
            return "Reportes\\";
        else return data[0][0].toString();
    }
    
    /**
     * Retorna la fecha que tiene la BD.
     * En caso de que el query no retorne nada
     * devolvera la hora actual en la maquina.
     * @param mysesion
     * @param mylogs
     * @return
     */
    public static java.util.Date Fecha(long mysesion,Logs mylogs){
        
        String SQL ="select curdate()";
        Object data[][]=OperacionesBD.Select(SQL, mylogs, mysesion);
        if(data.length==0){
            return new java.util.Date();
        }else{
            return  CambiarDateDeSqlaUtil((java.sql.Date)data[0][0]);
        }
        
    }
    
     /**
     * Convierte una fecha del java.sql. a una fecha a java.util
     * @param fecha: la fecha en java.util que se desea convertir
     * @return el equivalente de  "fecha"  como una fecha en java.sql.Date
     */
    public static java.util.Date CambiarDateDeSqlaUtil(java.sql.Date fecha){
        try{
            return new Date(fecha.getTime());
        }catch(Exception e){
            return null;
        }
    }
    /**
     * retorna la fecha actual que esta en la BD.
     * @param mysesion
     * @param mylogs
     * @param sql es para especificar qeu debe rtornar una fecha SQL y diferenciarlo
     * java.util.Date Fecha(long mysesion,Logs mylogs)
     * @return
     */
    public static java.sql.Date Fecha(long mysesion,Logs mylogs,boolean sql){
        
        String SQL ="select curdate()";
        Object data[][]=OperacionesBD.Select(SQL, mylogs, mysesion);
        if(data.length==0){
            return CambiarDateDeUtilaSql(new java.util.Date());
        }else{
            return  (java.sql.Date)data[0][0];
        }
        
    }
        
    /**
     * Convierte una fecha del java.util a una fecha a java.sql
     * @param fecha: la fecha en java.util que se desea convertir
     * @return el equivalente de  "fecha"  como una fecha en java.sql.Date
     */
    public static java.sql.Date CambiarDateDeUtilaSql(java.util.Date fecha){
        try{
            return new Date(fecha.getTime());
        }catch(Exception e){
            return null;
        }
    }
    /**
     * @return retorna la Hora del sistema en un String.
     *
     */
    public static String Hora(){
        Calendar calendario = Calendar.getInstance();
        int hora, minutos, segundos;
        hora =calendario.get(Calendar.HOUR);
        minutos = calendario.get(Calendar.MINUTE);
        segundos = calendario.get(Calendar.SECOND);
        
        return (hora) + ":" + minutos + ":" + segundos+" "+(calendario.get(Calendar.HOUR_OF_DAY)>12?" PM":" AM");
    }
    
    
    /**
     * se encarga de cambiar el modelo de la tabla miTable,
     * utilizando como parametros,la data las classes, los nombres y los editables.
     * @param data
     * @param miTable
     * @param calss
     * @param nombres
     * @param editables
     */
    public static void CambiarModelo(Object data[][],JTable miTable,Class calss[],
            String nombres[],boolean editables[]){
        modeloTabla miModelo=new modeloTabla(data, calss, nombres, editables);
        miTable.setModel(miModelo);
        try{
            miTable.addRowSelectionInterval(0, 0);
            JXTable t=(JXTable) miTable;
            t.packAll();
            t.addRowSelectionInterval(0, 0);
            
            
        }catch(Exception e){
            int x=0;
        }
    }
    /**
     * se encarga de cambiar el modelo de la tabla miTable,
     * utilizando como parametros,la data las classes, los nombres y los editables.
     * @param data
     * @param miTable
     * @param calss
     * @param nombres
     * @param editables
     * @param visibles
     */
    public static void CambiarModelo(Object data[][],JTable miTable,Class calss[],
            String nombres[],boolean editables[], boolean visibles[]){
        modeloTabla miModelo=new modeloTabla(data, calss, nombres, editables, visibles);
        miTable.setModel(miModelo);
        try{
            miTable.addRowSelectionInterval(0, 0);
            JXTable t=(JXTable) miTable;
            
            t.addRowSelectionInterval(0, 0);
            int i=0;
            for(boolean visible:visibles){
                if(!visible&&(miModelo.nombres.length!=1&&!miModelo.nombres[0].startsWith("No se produjo "))){
                    Ocultar_Columna_tabla(miTable,i);
                }
                i++;
            }
            t.packAll();
        }catch(Exception e){
            
        }
    }
    /**
     * oculta una columna de una tabla
     * @param tabla: a la que se le ocultará la columna
     * @param col: columna a ocultar
     */
    public static void Ocultar_Columna_tabla(JTable tabla,int col){
        if(col<0){
            return ;
        }
        if(col>=tabla.getColumnCount()){
            return ;
        }
        
        
        tabla.getColumnModel().getColumn(col).setMinWidth(0);
        tabla.getColumnModel().getColumn(col).setMaxWidth(0);
    }
    
    /**
     * se encarga de cambiar el modelo de la tabla miTable,
     * utilizando como parametros,la data las classes y los nombres.
     * @param data
     * @param miTable
     * @param calss
     * @param nombres
     */
    public static void CambiarModelo(Object data[][],JTable miTable,Class calss[],
            String nombres[]){
        
        modeloTabla miModelo=new modeloTabla(data, calss, nombres);
        miTable.setModel(miModelo);
        try{
            JXTable t=(JXTable) miTable;
            t.packAll();
            
            miTable.addRowSelectionInterval(0, 0);
            
        }catch(Exception e){
            
        }
    }
    
    
    
    /**
     * se encarga de cambiar el modelo de la tabla miTable,
     * utilizando como parametros,la data las classes y los nombres.
     * @param data
     * @param miTable
     * @param nombres
     */
    public static void CambiarModelo(Object data[][],JTable miTable,String nombres[]){
        Class clases[]=new Class[nombres.length];
        for (int i = 0; i < nombres.length; i++) {
            clases[i]=String.class;
        }
        modeloTabla miModelo=new modeloTabla(data, clases, nombres);
        miTable.setModel(miModelo);
        try{
            JXTable t=(JXTable) miTable;
            t.packAll();
            
            miTable.addRowSelectionInterval(0, 0);
            
        }catch(Exception e){
            
        }
    }
    
    
 
   /**
    * Este metodo se encarga de recibir un arraylist y lo convierte en 
    * un arreglo de objetos, Ojo si se quiere un arreglo de String se puede llamar
    * al metodo Convierte_arreglo_objeto_a_String(array) de esta misma clase
    * @param mylogs
    * @param mysesion
    * @param arreglo
    * @return UN arreglo de objetos con todos los valores del arrayList que se 
    * le pasa como parametro.
    *
    */
    public static Object[] Convertir_de_array_list_a_arreglo_objetos(Logs mylogs, long mysesion,ArrayList arreglo){
         
        ArrayList respuesta_array=new  ArrayList();
         
        Iterator lineas=arreglo.iterator();
        while(lineas.hasNext()){
            respuesta_array.add(lineas.next());   
        }
        Object respuesta[]=new Object[respuesta_array.size()];
        int i=0;
        lineas=respuesta_array.iterator();
        while(lineas.hasNext()){
            
            respuesta[i]=(Object) lineas.next();
            i++;
            
        }
        return respuesta;
    }
    
    
       public static boolean valueInArray(int i, int[] rowIndexes) {
        for (int j = 0; j < rowIndexes.length; j++) {
            if(rowIndexes[j]==i){
                return true;
            }
        }
        return false;
    }
       
       /**
     * Este metodo se encarga de revisar si el String texto solo tiene caracteres blancos.
     *
     * @param texto: el String que se desea revisar
     * @return true si esta en blanco false si al menos uno de los caracteres no es
     * un blanco.
     */
    public static boolean EstaEnBlanco(String texto){
        if(texto==null) return true;
        if(texto.length()==0){
            return true;
        }
        for (int i = 0; i < texto.length(); i++) {
            
            if(texto.charAt(i)!=' '){
                return false;
            }
        }
        return true;
        
    }
    
    /**
     * Este metodo convierte un arreglo de objetos a un arreglo de string
     * @param array el arreglo a convertir
     * @return un arreglo con el equivalente a string del objero array
     */
    
    public static  String[] Convierte_arreglo_objeto_a_String(Object array[]){
        String res[]=new String[array.length];
        
        for (int i = 0; i < array.length; i++) {
            res[i]=array[i]==null?null:array[i].toString();
        }
        return res;
    }
}






