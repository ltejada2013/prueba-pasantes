/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exmen;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
 
public class TableExample extends JFrame
{
    public TableExample()
    {
        //headers for the table
        String[] columns = new String[] {
            "Id", "matricula", "nombre", "apellido"
        };
        Class clases_columnas_tabla[]={Long.class,String.class,String.class,String.class};
        boolean edit[] = { false, true, false, false};
        boolean visibles[] = { false, false, false, true};
        //actual data for the table in a 2d array
        String sql="Select * from test.estudiantes";
        Object data[][]=OperacionesBD.Select(sql,new Logs(),1);
        
        modeloTabla model = new modeloTabla(data, clases_columnas_tabla, columns, edit, visibles);
        //create table with data
        JTable table = new JTable(model);
         
        //add the table to the frame
        this.add(new JScrollPane(table));
         
        this.setTitle("Table Example");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);       
        this.pack();
        this.setVisible(true);
    }
     
    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new TableExample();
            }
        });
    }   
}
