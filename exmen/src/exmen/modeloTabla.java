/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/
package  exmen;

import java.awt.Component;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 *
 * @author Administrador
 */
public class modeloTabla implements TableModel{
    
    public int row;
    public int column;
    public Object Data[][];
    private Object OriginalData[][];
    boolean  misEditables[];
    boolean  misVisibles[];
    Class clases[];
    String nombres[];
    TableModelListener listenerList;
    public  Object valorViejo;
    /**
     * Este objeto se utilizara cuando se actualice una fila de una tabla
     * usando el metodo (setvalueAt) esto es para poder ser utilizado cuando
     * se llame la linea listenerList.tableChanged(tme);
     * solo hay que (en el metodo tableChanged):
     *
     *
     */
    public Object ValorNuevo;
    
    /**
     * constructor por defecto, esto es solo un porsiacaso.
     */
    public modeloTabla() {
        DOthis();
        misVisibles= new boolean[column];
        for (int i = 0; i < misVisibles.length; i++) {
            misVisibles[i]=true;
        }
        
    }
    /**
     * este constructor sera utilizado cuando se espere
     * real data to be showed on the table.
     * @param data
     * @param edi
     * @param cl
     * @param nom
     */
    public modeloTabla(Object data[][],Class cl[],String nom[]){
        if(data.length!=0){
            row=data.length;
            column=cl.length;
            Data=data;
            OriginalData=data;
            clases=cl;
            nombres=nom;
            cambiar_longs_por_booleanos();
            misVisibles= new boolean[column];
            for (int i = 0; i < misVisibles.length; i++) {
            misVisibles[i]=true;
        }
        }else{
            DOthis();
        }
        
    }
    /**
     * este contructor se diferencia del anterior porque me permite
     * especificar si las columnas son editables o no
     * @param data
     * @param cl
     * @param nom
     * @param edi
     */
    public modeloTabla(Object data[][],Class cl[],String nom[],boolean  edi[]){
        if(data.length!=0){
            row=data.length;
            column=cl.length;
            Data=data;
            OriginalData=data;
            clases=cl;
            nombres=nom;
            misEditables=edi;
            misVisibles= new boolean[column];
            for (int i = 0; i < misVisibles.length; i++) {
            misVisibles[i]=true;
        }
            cambiar_longs_por_booleanos();
        }else{
            DOthis();
        }
        
    }
    
    /**
     * este contructor se diferencia del anterior porque me permite
     * especificar si las columnas son editables o no
     * @param data
     * @param cl
     * @param nom
     * @param edi
     * @param visibles indica si las columnas seran visibles.
     */
    public modeloTabla(Object data[][],Class cl[],String nom[],boolean  edi[], boolean visibles[]){
        if(data.length!=0){
            row=data.length;
            column=cl.length;
            Data=data;
            OriginalData=data;
            clases=cl;
            nombres=nom;
            misEditables=edi;
            misVisibles=visibles;
           
            cambiar_longs_por_booleanos();
        }else{
            DOthis();
        }
        
    }
    
    /**
     * este contructor se diferencia del anterior porque me permite
     * especificar si las columnas son editables o no
     * @param SQL esta es la consulta SQL que se desea realizar
     * @param cl las clases que representa cada una de las columnas de la tabla
     * @param edi que columnas seran editables y cuales no.
     * @param sesion sesion que llamo este constructor
     * @param logs los logs donde se guardaran estos logs
     */
    public modeloTabla(String SQL,Class cl[],boolean  edi[],long sesion,Logs logs){
        /*if(data.length!=0){
        row=data.length;
        column=cl.length;
        Data=data;
        clases=cl;
        nombres=nom;
        misEditables=edi;
        }else{
        DOthis();
        }*/
        
    }
    
    
    /**
     * este metodo es solo para poder usar las declaraciones que estan en
     * el constructor, ya que no se como hacerlo :P
     *
     */
    public  void DOthis(){
        row=1;
        column=1;
        Data=new Object[row][column];
        
        clases=new Class[]{String.class};
        nombres=new String[]{"No se produjo ningún resultado para esta búsqueda"};
        
        if(misVisibles!=null)  misVisibles[0]=true;
        
    }
    public Object [][]DameData(){
        return Data;
    }
    
    
    @Override
    public int getRowCount() {
        return row;
    }
    
    @Override
    public int getColumnCount() {
        return column;
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        return nombres[columnIndex];
    }
    
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        
        return clases[columnIndex];
    }
    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if(misEditables!=null){
            return misEditables[columnIndex];
        }
        return false;
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        
        
        if(Data==null||rowIndex>=row||columnIndex>=column){
            return null;
        }
        return Data[rowIndex][columnIndex];
    }
    
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        
        try{
            valorViejo=Data[rowIndex][columnIndex];
            Data[rowIndex][columnIndex]=aValue;
        }catch(Exception e){
            
        }
        if(listenerList!=null){
            ValorNuevo=aValue;
            TableModelEvent tme=new TableModelEvent(this, rowIndex,rowIndex,columnIndex);
            
            listenerList.tableChanged(tme);
        }
    }
    
    @Override
    public void addTableModelListener(TableModelListener l) {
        listenerList=l;
    }
    
    @Override
    public void removeTableModelListener(TableModelListener l) {
        
    }
    
    
    public Component componentAt(int row,int column){
        return (Component)Data[row][column];
    }
    
    /**
     * Anade una nueva fila con valores nulos al objeto de esta clase
     */
    public void AddRow(){
        Object Data1[][] =new Object[row + 1][column];
        System.arraycopy(Data, 0, Data1, 0, Data.length);
        Data = Data1;
        /*          for (int i = 0; i < Data1.length; i++) {
        for (int j = 0; j < Data[0].length; j++) {
        // System.out.println(Data[i][j]);
        }
        
        }
        */        row=row+1;
        
    }
    
    /**
     * Elimina la fila en la posicion rowIndex
     * @param rowIndex numero de la fila que se desea eliminar.
     */
    public void RemoveRow(int rowIndex){
        if(rowIndex>=row){
            return ;
        }
        
        Object data[][]=new Object[row-1][column];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                if(i<rowIndex){
                    data[i][j]=Data[i][j];
                }else if(i==rowIndex){
                    
                }else if(i>rowIndex){
                    data[i-1][j]=Data[i][j];
                }
            }
        }
        row--;
        Data=data;
        if(listenerList!=null){
            TableModelEvent tme=new TableModelEvent(this, 0, row, TableModelEvent.ALL_COLUMNS, TableModelEvent.DELETE);
            
            listenerList.tableChanged(tme);
        }
    }
    
    
    /**
     * Elimina la fila en la posicion rowIndex
     * @param rowIndex numero de la fila que se desea eliminar.
     */
    public void RemoveAllRows(){
        Object data[][]=new Object[0][column];
        
        row=0;
        Data=data;
    }
    
    /**
     * Este metodo elimina todos las filas que esten en en el arreglo rowindexes
     * @param rowIndexes: arreglo que contiene los indexes que se quieren borrar.
     */
    public void RemoveRows(int rowIndexes[]){
        Object data[][]=new Object[row-rowIndexes.length][column];
        int pos=0;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                
                if(FuncionesGenerales.valueInArray(i, rowIndexes)){
                    continue;
                }else{
                    data[pos][j]=Data[i][j];
                }
            }
            if(!FuncionesGenerales.valueInArray(i, rowIndexes)){
                pos++;
            }
        }
        row-=rowIndexes.length;
        Data=data;
        if(listenerList!=null){
            TableModelEvent tme=new TableModelEvent(this, 0, row, TableModelEvent.ALL_COLUMNS, TableModelEvent.DELETE);
            
            listenerList.tableChanged(tme);
        }
    }
    
    
    /**
     * Este metodo anade una nueva fila a la tabla, los valores de la  nueva
     * fila estan en newRow
     * @param newRow valores que se desean agregar a la nueva fila
     */
    public void AddRow(Object newRow[]){
        Object Data1[][] =new Object[row +1][column];
        System.arraycopy(Data, 0, Data1, 0, Data.length);
        Data = Data1;
        Data[row]=newRow;
        row+=1;
        
    }
    
    /**
     * Este metodo anade una nueva fila a la tabla, los valores de la  nueva
     * fila estan en newRow
     * @param newRows valores que se desean agregar a la nueva fila
     */
    public void AddRow(Object newRows[][]){
        Object Data1[][] =new Object[row +newRows.length][column];
        System.arraycopy(Data, 0, Data1, 0, Data.length);
        Data = Data1;
        System.arraycopy(newRows, 0, Data, row, newRows.length);
        row+=newRows.length;
        
    }
    /**
     * Este metodo se encarga de revisar si todos los registros de la
     * tabla son null, si todos son nulls entonces retornara true
     * o en caso de que no tenga filas.
     * @return true si todo es null false si no
     */
    public boolean Esta_Todo_en_Null(){
        if(row==0){
            return true;
        }
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                if(Data[i][j]!=null)
                    return false;
            }
        }
        return true;
    }
    
    /**
     * Este metodo se encarga de filtrar la informacion que sera visible en una tabla
     * basado en la data que este en la columna columna_a_filtrar, la misma debera de contener
     * el texto en la variable text para que sea visible en dicha tabla.
     * Ojo la idea es ocultar las filas que no cumplan con el filtro no borrarlas.
     * @param columna_a_filtrar: el indice de la columna en donde esta el filtro.
     * @param text : el texto que se desea filtrar.
     *
     * A continuacion un ejemplo funcional de como utilizar este metodo
     *
     *
     *          modeloTabla m =(modeloTabla)jXTable1.getModel();
     * m.Filtar(0, tfactura.getText());
     * jXTable1.setModel(m);
     * jXTable1.repaint();
     */
    public void Filtar_columna(int columna_a_filtrar,String text){
        if(text.length()==0|| columna_a_filtrar>column){
            //Si el tamano del texto es 0 entonces se muestra todo lo que esta en el
            // arreglo original o si las columna a filtrar es mayor que el numero de
            //columnas del arreglo originaldata
            Data=OriginalData;
            row=Data.length;
        }else{
            //declarar un arreglo booleano con el tota de elementos
            boolean filtrado[]=new boolean [OriginalData.length];
            int new_rows=0;
            for(int i=0;i<OriginalData.length;i++){
                //recorrer la matriz OriginalData
                String temp=OriginalData[i][columna_a_filtrar].toString();
                filtrado[i]=temp.toLowerCase().contains(text.toLowerCase());
                if(filtrado[i]){
                    //si la fila i cumple con las condiciones de filtradoo,
                    //se guarda true en la posicion i del arreglo filtrado
                    //se incrementa en 1 el numero real de nuevas filas
                    new_rows++;
                }
            }
            //se declara el objeto Data con el numero nuevo de filas
            // y se cambia la variable row al nuevo valor que este debe tener
            Data= new Object[new_rows][column];
            row=new_rows;
            
            int i=0;
            for (int j = 0; j < filtrado.length; j++) {
                //recorrer el arreglo filtrado y si en alguna posicion es =true
                // asignar a Data lo que tiene OriginalData en esa posicion.
                if(filtrado[j]){
                    Data[i]=OriginalData[j];
                    i++;
                }
            }
            
        }
    }
    
    
    /**
     * Este metodo se encarga de copiar la tabla en el clipboard para pegarlo en
     * un archivo de excel.
     * o cualquier otro lugar si asi se desea.
     * Este copia el nombre de las columnas y luego copia la info en la tabla.
     */
    public void Copiar_data_a_clipBoard(){
        
        String resultado="";
        
        for (String nombre:nombres) {
            resultado+=nombre+"\t";
        }
        resultado+="\n";
        
        for (Object datas[]: Data) {
            for (Object data: datas) {
                resultado+=data==null?"\t":data.toString()+"\t";
            }
            resultado+="\n";
        }
        
        
        StringSelection stringSelection = new StringSelection (resultado);
        Clipboard clpbrd = Toolkit.getDefaultToolkit ().getSystemClipboard ();
        clpbrd.setContents (stringSelection, null);
    }
    
    
        /**
     * Este metodo suma todos los valores de la columna que se paso como parametro
     * y retorna un l de dicha sumatoria
     * EJ: modeloTabla m = (modeloTabla)jXTable1.getModel();
            lcantidad.setText(""+m.Suma_Columna_long(2));
     * @param columna: indixe de la columna (el 0 esta incluido) que se desea la sumatoria
     * @return la sumatoria de la columna recivida como parametro
     */
    
    public long Suma_Columna_long(int columna ){
        long resp = 0;
        if (columna >= this.column || columna < 0)
        {
            return 0; }
        for (int i = 0; i < row; i++)
        {
            try{
                resp += Long.parseLong(Data[i][columna].toString());
            }catch(Exception e){
                JOptionPane.showMessageDialog(new JFrame(),"Error al hacer la sumatoria.","",JOptionPane.ERROR_MESSAGE);
                return  0;
            }
            
            
            
            
        }
        return resp;
        
    }
    /**
     * Este metodo suma todos los valores de la columna que se paso como parametro
     * y retorna un double de dicha sumatoria
     * EJ: modeloTabla m = (modeloTabla)jXTable1.getModel();
            lcantidad.setText(""+m.Suma_Columna_long(2));
     * @param columna: indixe de la columna (el 0 esta incluido) que se desea la sumatoria
     * @return la sumatoria de la columna recivida como parametro
     */
    public double Suma_Columna_double(int columna){
        double resp = 0;
        if (columna >= this.column || columna < 0)
        {
            return 0; }
        for (int i = 0; i < row; i++)
        {
            
            try{
                resp += Double.parseDouble(Data[i][columna].toString());
            }catch(Exception e){
                JOptionPane.showMessageDialog(new JFrame(),"Error al hacer la sumatoria de la columna "+(columna+1)+".","",JOptionPane.ERROR_MESSAGE);
                return  0;
            }
            
            
        }
        return resp;
        
    }
    
    /**
     * Este metodo suma todos los valores de la columna que se paso como parametro
     * y retorna un entero de dicha sumatoria
     * EJ: modeloTabla m = (modeloTabla)jXTable1.getModel();
            lcantidad.setText(""+m.Suma_Columna_long(2));
     * @param columna: indixe de la columna (el 0 esta incluido) que se desea la sumatoria
     * @return la sumatoria de la columna recivida como parametro
     */
    public int Suma_Columna_int(int columna){
        int  resp = 0;
        if (columna >= this.column || columna < 0)
        {
            return 0; }
        for (int i = 0; i < row; i++)
        {
            
            
            try{
                resp += Integer.parseInt(Data[i][columna].toString());
            }catch(Exception e){
                JOptionPane.showMessageDialog(new JFrame(),"Error al hacer la sumatoria.","",JOptionPane.ERROR_MESSAGE);
                return  0;
            }
            
            
            
        }
        return resp;
        
    }
    
    /**
     * Este metodo recibe un row index y una linea completamente nueva
     * la cual remplazara la antigua.
     * Tomar en cuenta que si el row index es <=0 no hara nada
     * o si es >= el numero total de filas que tiene la tabla, ademas si el 
     * tamano de la linea nueva y el de la linea vieja son diferentes.
     * 
     * @param linea, arreglo con los nuevos valores que tendra la nueva linea
     * @param row_index, numero de la linea que sera remplazada.
     */
    public void Replace_Row(Object []linea, int row_index){
       if(row_index<0)
           return ;
       if(row_index>=row)
           return ;
       
       try{
          if(Data[row_index].length!=linea.length)
              return ;
       }catch(Exception e){
           return ;
       }
        Data[row_index]=linea;
    }
    
    
    /**
     * Este metodo sirve para evitar muchos problemas que cause tener una columna
     * boolean cuando se trae desde mysql puesto que mysql retorna un long,
     * asi que para asegurar que en la tabla hay un valor booleano en las columnas
     * que deben ser booleanos entonces se procede a especificar el valor basado
     * claro está en la data original
     * 
     */
        public void cambiar_longs_por_booleanos(){
        
        
        for (int clase = 0; clase < clases.length; clase++) {
            if(clases[clase].equals(Boolean.class)){
                
                for (int fila = 0; fila < Data.length; fila++) {
                    
                    
                     boolean valor_booleano=false;

                        //A veces funciona con boolean otras veces con long
                        //asi que para tener cambas posibilidades lo hice así
                        try{
                            valor_booleano=(boolean)(Data[fila][clase]);
                        }catch(Exception e){
                            try{
                            valor_booleano=(long)(Data[fila][clase]==null?0:Data[fila][clase])>0;
                            }catch(Exception ee){
                                valor_booleano=(int)(Data[fila][clase]==null?0:Data[fila][clase])>0;
                            }
                        }
                        Data[fila][clase]=valor_booleano;
                    
                }
               
            }
        }
        
    }
    
        /**
         * Este metodo retorna un arreglo con todos lovalor ede la columna que
         * se desea
         * @param column in indice de la columna que queremos pasar.
         * @param mylogs
         * @param myPadre
         * @return Un arreglo de objetos que contiene todos los valores en la
         * columna que se pasa como parametro.
         */
        public Object []getColumn(int column,Logs mylogs,JFrame myPadre){
            Object respuesta[]=new Object[row];
            
            try{
                for (int i = 0; i < row; i++) {
                    respuesta [i]=Data[i][0];
                }
            }catch(Exception e){
             
            }
            return respuesta;
        }
}