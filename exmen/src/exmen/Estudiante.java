/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exmen;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ltejada
 */
public class Estudiante {
    
    
    private int matricula;
    private String nombre;

    private  int ID=1;
    private String apellido;
    private String telefono;

    public int getMyId() {
        return myId;
    }
    private  int myId=ID;
    
    public Estudiante(int matricula, String nombre, String apellido, String telefono) {
        this.matricula = matricula;
        this.nombre = nombre;
        this.apellido = apellido;
        this.telefono = telefono;
    //    ID++;
    }
    
    public Estudiante(int matricula, String nombre, String apellido) {
        this.matricula = matricula;
        this.nombre = nombre;
        this.apellido = apellido;
      //  ID++;
    }
    
    public Estudiante(){
        
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTelefono() {
        return telefono==null?"":telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
      public String getMatriculaString()
      {
          return (matricula+"").replace(",","");
      }
   
      
      public void setId(int id){
          this.myId=id;
      }
      
    public static void main(String[] args) {
        List<Estudiante> estudiantes= new ArrayList();
        Estudiante a= new Estudiante(100, "Liz", "Tejada");
        
        Estudiante b= new Estudiante(11, "Lourdes", "FErnandez");
        
        Estudiante c= new Estudiante(12, "Nairobi", "Almonte");
        
        estudiantes.add(b);
        estudiantes.add(a);
        estudiantes.add(c);
        /*
        
        Complete el codigo en esta clase (Puedes modificar o agregar codigo 
        fuera del main) para que en el for mas abajo la impresion de los 
        elementos de la Lista salgan ordenados en forma descendente basados en
        la matricula.
        
        Se espera el siguiente output:
        Liz TEjada 100
        Nairobi Almonte 12
        Loureds Fernande 11
        
        */
       
       for(Estudiante e:estudiantes){
            System.out.println(e.getNombre()+" "+e.getApellido()+" "+e.getMatriculaString());
        };
        
    }

   
 
      
      
      
}

