/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exmen;

/**
 * Ultima vez modificado 2013-04-19
 *Esta interface es una interfaz considero mas util que cual es el evento 
 * puesto que me permitira tener un Id por lo menos y asi no tendré que tener 
 * una referencia de la clase que utilice esta interface
 * @author Liz Tejada
 */
public interface PasarUnId {
    
    
    
    
    public void YaTengoId(long myid);
    public void YaTengoId(long myid,String nombre);    
    
    
}
