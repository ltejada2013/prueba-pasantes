/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package exmen;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author ltejada
 */
public class my_icons {
    
    
    
    public my_icons(){
        try{
            
            agregar_3D_16x16= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_16x16_Add.png"));
            guardar_3D_16x16= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_16x16_Save.png"));
            eliminar_3D_16x16= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_16x16_Minus.png"));
            lupa_3D_16x16= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_16x16_View.png"));
            buscar_3D_16x16= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_16x16_Search.png"));
            back_3D_16x16= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_16x16_Back.png"));
            pregunta_16x16= new ImageIcon(getClass().getResource("/MyClasses/resources/Help_Circle_Blue.png"));
            agregar_16x16= agregar_3D_16x16;//new ImageIcon(getClass().getResource("/MyClasses/resources/Circle_Blue.png"));System.out.println("1");
            calendario_16x16= new ImageIcon(getClass().getResource("/MyClasses/resources/calendar.png"));
            cerrar_3D_16x16= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_16x16_Close.png"));
            refrescar_3D_16x16= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_16x16_Refresh.png"));
            information_3D_16x16= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_16x16_Information.png"));
            user_3D_16x16= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_16x16_Run.png"));
            options_3D_16x16= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_16x16_Options.png"));
            about_3D_16x16= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_16x16_About.png"));
            help_3D_16x16= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_16x16_Help.png"));
            refresh_3D_16x16= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_16x16_Refresh.png"));
            crear_3D_16x16= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_16x16_Create.png"));
            ver_3D_16x16= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_16x16_View.png"));
            adelante_3D_16x16= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_16x16_Next.png"));
            run_3D_16x16= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_16x16_Run.png"));
            check_3D_16x16= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_16x16_Accept.png"));
            borrar_3D_16x16= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_16x16_Erase.png"));
             modificar_columnas_visibles_tabla_3D_16x16= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_16x16_System.png"));
            
            buscar_32x32=buscar_3D_32x32;// new ImageIcon(getClass().getResource("/MyClasses/resources/PNG_32x32_search.png"));
            stop_32x32= stop_3D_32x32;//new ImageIcon(getClass().getResource("/MyClasses/resources/PNG_32x32_stop_alt.png"));
            stop1_32x32= stop_3D_32x32;//new ImageIcon(getClass().getResource("/MyClasses/resources/PNG_32x32_stop.png"));
            stop2_32x32= stop_3D_32x32;//new ImageIcon(getClass().getResource("/MyClasses/resources/PNG_32x32_stop2.png"));
            carita_feliz_32x32= stop_3D_32x32;//new ImageIcon(getClass().getResource("/MyClasses/resources/PNG_32x32_smiley.png"));
            carita_triste_32x32= stop_3D_32x32;//new ImageIcon(getClass().getResource("/MyClasses/resources/PNG_32x32_smileysad.png"));
            editar_32x32=  new ImageIcon(getClass().getResource("/MyClasses/resources/32x32_edit.png"));          
            
            
            buscar_3D_32x32= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_32x32_Search.png"));
            stop_3D_32x32= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_32x32_Stop.png"));
            guardar_3D_32x32= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_32x32_Save.png"));
            cerrar_3D_32x32= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_32x32_Close.png"));
            refrescar_3D_32x32= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_32x32_Refresh.png"));
            information_3D_32x32= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_32x32_Information.png"));
            back_3D_32x32= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_32x32_Back.png"));
            user_3D_32x32= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_32x32_User.png"));
            options_3D_32x32= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_32x32_Options.png"));
            about_3D_32x32= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_32x32_About.png"));
            help_3D_32x32= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_32x32_Help.png"));
            refresh_3D_32x32= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_32x32_Refresh.png"));
            crear_3D_32x32= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_32x32_Create.png"));
            ver_3D_32x32= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_32x32_View.png"));
            adelante_3D_32x32= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_32x32_Next.png"));
            run_3D_32x32= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_32x32_Run.png"));
            check_3D_32x32= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_32x32_Accept.png"));
            borrar_3D_32x32= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_32x32_Erase.png"));
            pulgar_arriba_3D_32x32= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_32x32_tumbs_up.png"));
            modificar_columnas_visibles_tabla3D_32x32= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_32x32_System.png"));
            
            
            abort_3D_64x64= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_64x64_Abort.png"));
            information_3D_64x64= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_64x64_Information.png"));
            information_64x64= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_64x64_info_blue.png"));
            user_3D_64x64= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_64x64_User.png"));
            options_3D_64x64= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_64x64_Options.png"));
            about_3D_64x64= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_64x64_About.png"));
            help_3D_64x64= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_64x64_Help.png"));
            refresh_3D_64x64= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_64x64_Refresh.png"));
            crear_3D_64x64= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_64x64_Create.png"));
            ver_3D_64x64= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_64x64_View.png"));
            adelante_3D_64x64= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_64x64_Next.png"));
            run_3D_64x64= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_64x64_Run.png"));
            check_3D_64x64= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_64x64_Accept.png"));
            borrar_3D_64x64= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_64x64_Erase.png"));
            modificar_columnas_visibles_tabla3D_64x64= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_64x64_System.png"));
            
            
            
            cancelar_3D_32x32= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_32x32_Cancel.png"));
            cancelar_3D_16x16= new ImageIcon(getClass().getResource("/MyClasses/resources/3D_16x16_Cancel.png"));
            
        }catch(Exception e){
            
            JOptionPane.showMessageDialog(new JFrame(), "Error iconos "+e.getMessage()
                    +"\n\"/MyClasses/resources/Add.png\"\n"+
                    System.getProperty("user.dir")
                    ,"",JOptionPane.ERROR_MESSAGE, abort_3D_64x64);
        }
    }
    
    
    public   ImageIcon agregar_3D_16x16;
    public   ImageIcon guardar_3D_16x16;
    public   ImageIcon eliminar_3D_16x16;
    public   ImageIcon lupa_3D_16x16 ;
    public   ImageIcon buscar_3D_16x16;
    public   ImageIcon back_3D_16x16;
    public   ImageIcon pregunta_16x16;
    public   ImageIcon agregar_16x16;
    public   ImageIcon calendario_16x16;
    public   ImageIcon cerrar_3D_16x16;
    public   ImageIcon refrescar_3D_16x16;
    public   ImageIcon information_3D_16x16;
    public   ImageIcon user_3D_16x16;
    public   ImageIcon options_3D_16x16;
    public   ImageIcon about_3D_16x16;
    public   ImageIcon help_3D_16x16;
    public   ImageIcon refresh_3D_16x16;
    public   ImageIcon crear_3D_16x16;
    public   ImageIcon ver_3D_16x16;
    public   ImageIcon adelante_3D_16x16;
    public   ImageIcon run_3D_16x16;
    public   ImageIcon cancelar_3D_16x16;
    public   ImageIcon check_3D_16x16;
    public   ImageIcon borrar_3D_16x16;
    public   ImageIcon modificar_columnas_visibles_tabla_3D_16x16;
    
    public   ImageIcon buscar_32x32;
    public   ImageIcon stop_32x32;
    public   ImageIcon stop1_32x32;
    public   ImageIcon stop2_32x32;
    public   ImageIcon carita_feliz_32x32;
    public   ImageIcon carita_triste_32x32;
    public   ImageIcon    editar_32x32;
    
    public   ImageIcon pulgar_arriba_3D_32x32;
    public   ImageIcon guardar_3D_32x32;
    public   ImageIcon cerrar_3D_32x32;
    public   ImageIcon refrescar_3D_32x32;
    public   ImageIcon information_3D_32x32;
    public   ImageIcon back_3D_32x32;
    public   ImageIcon user_3D_32x32;
    public   ImageIcon options_3D_32x32;
    public   ImageIcon about_3D_32x32;
    public   ImageIcon help_3D_32x32;
    public   ImageIcon refresh_3D_32x32;
    public   ImageIcon crear_3D_32x32;
    public   ImageIcon ver_3D_32x32;
    public   ImageIcon adelante_3D_32x32;
    public   ImageIcon run_3D_32x32;
    public   ImageIcon buscar_3D_32x32;
    public   ImageIcon stop_3D_32x32;
    public   ImageIcon cancelar_3D_32x32;
    public   ImageIcon check_3D_32x32;
    public   ImageIcon borrar_3D_32x32;
    public   ImageIcon modificar_columnas_visibles_tabla3D_32x32;
    
    public   ImageIcon abort_3D_64x64;
    public   ImageIcon information_3D_64x64;
    public   ImageIcon information_64x64;
    public   ImageIcon user_3D_64x64;
    public   ImageIcon options_3D_64x64;
    public   ImageIcon about_3D_64x64;
    public   ImageIcon help_3D_64x64;
    public   ImageIcon refresh_3D_64x64;
    public   ImageIcon crear_3D_64x64;
    public   ImageIcon ver_3D_64x64;
    public   ImageIcon adelante_3D_64x64;
    public   ImageIcon run_3D_64x64;
    public   ImageIcon check_3D_64x64;
    public   ImageIcon borrar_3D_64x64;
    public   ImageIcon modificar_columnas_visibles_tabla3D_64x64;
    
}
