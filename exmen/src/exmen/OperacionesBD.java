/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package exmen;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Liz Tejada
 */
public class OperacionesBD {

    //192.168.1.2
    public static String DRIVER = "org.gjt.mm.mysql.Driver";

//public static String Connection="jdbc:mysql://192.168.1.2/cencarci1";  
//public static String Connection="jdbc:mysql://10.0.0.131/cencarci1";
    public static String Connection = "jdbc:mysql://localhost/cencarci1";
    public static String user = "root";
    public static String password = "mysql";

    /**
     * Este metodo se encarga de crear una sesion, agregando una entrada a la
     * tabla sesiones y otra entrada a la tabla Login. Especificando la s
     *
     * @param usuario: quien se quiere logear
     * @param l: ventana Logs
     * @param sesion1: la sesion que se quiere hacer este login
     * @param donde: esto es para tener un control en la tabla login
     * @return la sesion que fue creada
     */
    static long CrearSesion(long usuario, Logs l, int sesion1, String donde, String Sucursal) {
        String Sentencia = "insert into sesiones (empleado,empresa,sucursal) "
                + "select idempleados,empresa,'" + Sucursal + "' from empleados\n"
                + "\n"
                + "where idEmpleados= " + usuario;
        //  System.out.println(Sentencia);
        if (!OperacionesBD.insert(Sentencia, 2, l)) {
            JOptionPane.showMessageDialog(new JFrame(), "Error creando session");
            return -1;
        }
        Sentencia = "select idsesion from sesiones where empleado= " + usuario
                + " order by idsesion desc;";

        // System.out.println(Sentencia);
        long sesion = Integer.parseInt(OperacionesBD.Select(Sentencia, l, sesion1)[0][0].toString());
        Sentencia = "INSERT INTO `cencarci1`.`login` (`Sesion`, `Donde`) VALUES (" + sesion
                + ", " + "\"" + donde + "\""
                + ");";
        OperacionesBD.insert(Sentencia, sesion, l);

        return sesion;
    }

    public OperacionesBD() {

    }

    /**
     * se encarga de hacer el llamado a la sentencia update en la base de datos
     *
     * @param Sentencia: la sentencia que se desea hacer
     * @param sesion
     * @param l
     * @return true si el update fue realizado con exito, false en caso
     * contrario
     */
    public static boolean Update(String SQL, long sesion, Logs l) {
        Connection conexion = null;
        Statement update = null;

        try {
            Class.forName(DRIVER);

            try {
                conexion
                        = DriverManager.getConnection(Connection, user, password);
                update = conexion.createStatement();

                update.execute(SQL);
                update.close();
                conexion.close();
                //  l.Guardar_logs_en_la_BD(SQL,FuncionesGenerales.Fecha()+" "+FuncionesGenerales.Hora()                        , ""+sesion, "UPDATE");
                return true;

            } catch (SQLException e) {
                l.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                        + e.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
                try {

                    update.close();
                    conexion.close();
                    return false;
                } catch (SQLException ex) {
                    if (!ex.getMessage().startsWith("Data truncation: Incorrect datetime value:")) {
                        l.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                                + ex.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
                    }
                }

                return false;
            }
        } catch (ClassNotFoundException e) {
            l.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                    + e.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());

            try {

                update.close();
                conexion.close();
                return false;
            } catch (SQLException ex) {
                l.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                        + ex.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
            }
            return false;

        }

    }

    /**
     * se encarga de hacer el llamado a la sentencia insert en la base de datos
     *
     * @param Sentencia: la sentencia que se desea hacer
     * @param mysesion
     * @param l
     * @return true si el insert fue realizado con exito, false en caso
     * contrario
     */
    public static boolean insert(String Sentencia, long mysesion, Logs l) {

        Statement update = null;
        Connection conexion = null;

        try {
            Class.forName(DRIVER);

            try {
                conexion
                        = DriverManager.getConnection(Connection, user, password);
                update = conexion.createStatement();

                update.executeUpdate(Sentencia);
                update.close();
                conexion.close();
                //l.Guardar_logs_en_la_BD(Sentencia,FuncionesGenerales.Fecha()+" "+FuncionesGenerales.Hora()                        , ""+mysesion, "INSERT");
                return true;

            } catch (SQLException e) {
                if (!e.getMessage().startsWith("Data truncation: Incorrect datetime value:")) {
                    try {
                        l.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                                + e.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
                        update.close();
                        //System.out.println(e.getMessage());
                        conexion.close();
                        return false;
                    } catch (SQLException ex) {
                        l.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                                + ex.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
                    }
                }
            }
        } catch (ClassNotFoundException e) {
            try {
                l.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                        + e.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
                update.close();
                conexion.close();
                return false;
            } catch (SQLException ex) {
                l.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                        + e.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
            }
            l.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                    + e.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
            return false;
        }

        return false;

    }

    /**
     *
     * Se encarga de hacer un select a la BD
     *
     * @deprecated Use this one instead Select(Sentencia, logs, sesion)
     * @param Sentencia: el select que se desea hacer
     * @param Count: es una sentecia que permite calcular la cantidad de filas
     * que tiene el select
     * @param Columns: la cantidad de columnas que tiene el select
     * @param l
     * @param sesion
     * @return una matriz de objectos con en la cual esta el resultado del
     * select
     */
    public static Object[][] Select(String Sentencia, String Count, int Columns, Logs l, long sesion) {
        //  l.Guardar_logs_en_la_BD(Sentencia,FuncionesGenerales.Fecha()+" "+FuncionesGenerales.Hora()
        //        , ""+sesion, "SELECT");
        Connection conexion = null;
        ResultSet rs = null;
        Object respuesta[][] = new Object[1][Columns];
        try {
            Class.forName(DRIVER);

            try {
                conexion
                        = DriverManager.getConnection(Connection, user, password);
                PreparedStatement select = conexion.prepareStatement(Sentencia);

                rs = select.executeQuery();
                rs = select.executeQuery(Count);
                rs.next();

                respuesta = new Object[rs.getInt(1)][Columns];

                rs = select.executeQuery(Sentencia);
                int i = 0;

                while (rs.next()) {

                    for (int j = 0; j < Columns; j++) {
                        try {
                            respuesta[i][j] = rs.getObject(j + 1);
                        } catch (Exception eee) {
                        }
                    }
                    i++;
                }

                rs.close();
                select.close();

                conexion.close();

            } catch (SQLException e) {
                try {
                    l.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                            + e.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
//                    rs.close();
                    conexion.close();

                } catch (SQLException ex) {
                    l.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                            + ex.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
                }
                l.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                        + e.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
                return new Object[0][0];
            }
        } catch (ClassNotFoundException e) {
            try {
                l.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                        + e.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
                rs.close();
                conexion.close();

            } catch (SQLException ex) {
                l.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                        + ex.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
            }
            l.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                    + e.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
            return new Object[0][0];
        }

        return respuesta;

    }

    /**
     * Se encarga de hacer un select a la BD
     *
     * @param l
     * @param sesion
     * @deprecated Use this one instead Select(Sentencia, logs, sesion)
     * @param Sentencia: el select que se desea hacer
     * @param row: la cantidad de filas que tiene el select
     * @param Columns: la cantidad de columnas que tiene el select
     * @return una matriz de objectos con en la cual esta el resultado del
     * select, -1 si ocurre algun error.
     */
    public static Object[][] Select(String Sentencia, int row, int Columns, Logs l, long sesion) {
        //  l.Guardar_logs_en_la_BD(Sentencia,FuncionesGenerales.Fecha()+" "+FuncionesGenerales.Hora()
        //        , ""+sesion, "SELECT");
        Object respuesta[][] = new Object[1][Columns];
        try {
            Class.forName(DRIVER);

            try {
                Connection conexion
                        = DriverManager.getConnection(Connection, user, password);
                PreparedStatement select = conexion.prepareStatement(Sentencia);

                ResultSet rs = select.executeQuery();

                respuesta = new Object[row][Columns];

                rs = select.executeQuery(Sentencia);
                int i = 0;

                while (rs.next()) {

                    for (int j = 0; j < Columns; j++) {
                        try {
                            respuesta[i][j] = rs.getObject(j + 1);
                        } catch (Exception eee) {
                        }
                    }
                    i++;
                }

                rs.close();
                select.close();

                conexion.close();

            } catch (SQLException e) {
                l.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                        + e.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
                return new Object[0][0];
            }
        } catch (ClassNotFoundException e) {
            l.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                    + e.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
            return new Object[0][0];
        }

        return respuesta;

    }

    /**
     * Este metodo es utilizadpo para cuando se va a llamar un procedure el cual
     * retorne una tabla
     *
     * @param sentencia: el procedure que se quiere llamar con sus respectivos
     * parmetros
     * @param l: Ventana que manejara los Logs que ocurran en el mismo
     * @param sesion; quien ha llamado a este procedure
     * @return una matriz con la tabla deseada.
     */
    public static Object[][] Call(String sentencia, Logs l, long sesion) {
        //Object respuesta[][]=new Object[1][Columns];

        try {
            Class.forName(DRIVER);

            try {
                Connection conexion
                        = DriverManager.getConnection(Connection, user, password);
                CallableStatement cl;
                conexion.setAutoCommit(false);
                cl = conexion.prepareCall(sentencia);
                cl.execute();
                ResultSet rs = cl.getResultSet();

                int filas = 0;
                try {
                    while (rs.next()) {
                        filas++;
                    }

                } catch (java.lang.NullPointerException noresultset) {
                    l.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                            + noresultset.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
                    return new Object[0][0];
                }
                while (rs.previous()) {

                }

                int columns = 0;
                try {
                    rs.next();
                    //  System.out.println(rs.getRow());
                    while (columns != -1) {
                        Object temp = rs.getObject(columns + 1);
                        columns++;
                    }
                } catch (Exception force) {
                    rs.previous();
                }
                //System.out.println(rs.getRow());
                if (filas == 0) {
                    rs.close();
                    cl.close();
                    conexion.close();
                    return new Object[0][0];
                }
                Object respuesta[][] = new Object[filas][columns];
                while (rs.next()) {
                    int i = rs.getRow() - 1;
                    for (int j = 0; j < columns; j++) {
                        respuesta[i][j] = rs.getObject(j + 1);
                    }
                }
                //  l.Guardar_logs_en_la_BD(sentencia,FuncionesGenerales.Fecha()+" "+FuncionesGenerales.Hora()
                //        , ""+sesion, "CALL");
                rs.close();
                cl.close();
                conexion.close();
                return respuesta;
            } catch (SQLException e) {
                l.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                        + e.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
                return new Object[0][0];
            }
        } catch (ClassNotFoundException e) {
            l.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                    + e.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
            return new Object[0][0];
        }

    }

    /**
     * este metodo se encarga de retorar la cantidad de filas que tiene un
     * select
     *
     * @param Sentencia: el select al cual se le quiere calcular la cantidad de
     * filas
     * @param l
     * @return el total de filas que tiene dicho select
     */
    public static int count(String Sentencia, Logs l) {
        int total = 0;

        try {
            Class.forName(DRIVER);

            try {
                Connection conexion
                        = DriverManager.getConnection(Connection, user, password);
                PreparedStatement select = conexion.prepareStatement(Sentencia);

                ResultSet rs = select.executeQuery();
                rs = select.executeQuery(Sentencia);
                int i = 0;

                while (rs.next()) {
                    total++;
                }

                rs.close();
                select.close();

                conexion.close();

            } catch (SQLException e) {
                System.err.println("Error  " + e.getMessage());
                return -1;
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return -1;
        }
        return total;
    }

    /**
     * se encarga de hacer el llamado a la sentencia insert en la base de datos
     * Se diferencia de la funcion insert en que esta no guarda el log en la BD.
     * de lo que se inserto.
     *
     * Este metodo fue creado porque se estaba haciendo un ciclo infinito cuando
     * se intentaba guardar los logs desde la ventana de logs. en la BD.
     *
     * @param Sentencia: la sentencia que se desea hacer
     * @return true si el insert fue realizado con exito, false en caso
     * contrario
     */
    public static boolean insert1(String Sentencia, long sesion, Logs l) {

        Statement update = null;
        Connection conexion = null;

        try {
            Class.forName(DRIVER);

            try {
                conexion
                        = DriverManager.getConnection(Connection, user, password);
                update = conexion.createStatement();

                update.executeUpdate(Sentencia);
                update.close();
                conexion.close();

                return true;

            } catch (SQLException e) {
                try {
                    l.talogs.append("\nError ocurrido en OperacionesBD1" + " \n"
                            + e.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
                    update.close();
                    conexion.close();
                    return false;
                } catch (SQLException ex) {
                    l.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                            + ex.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
                }
            }
        } catch (ClassNotFoundException e) {
            try {
                l.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                        + e.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
                update.close();
                conexion.close();
                return false;
            } catch (SQLException ex) {
                l.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                        + e.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
            }
            l.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                    + e.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
            return false;
        }

        return false;

    }

    /**
     *
     * @param Sentencia: la consulta que se desea realiaz
     * @param l: el logs donde se va a guardar cualquier error que ocurra.
     * @param sesion: quien realizo el query
     * @return una matriz de Objetos donde se guarda la "tabla" que retorna la
     * consulta
     */
    public static Object[][] Select(String SQL, Logs l, long sesion) {

        Object respuesta[][] = new Object[1][1];
        try {
            Class.forName(DRIVER);

            try {
                Connection conexion
                        = DriverManager.getConnection(Connection, user, password);
                PreparedStatement select = conexion.prepareStatement(SQL);

                ResultSet rs = select.executeQuery();
                int filas = 0;
                try {
                    while (rs.next()) {
                        filas++;

                    }
                } catch (Exception e) {

                }
                while (rs.previous()) {

                }
                int columns = 0;
                try {
                    rs.next();
                    //  System.out.println(rs.getRow());
                    while (columns != -1) {
                        Object temp = rs.getObject(columns + 1);
                        columns++;
                    }
                } catch (Exception force) {
                    rs.previous();
                }
                respuesta = new Object[filas][columns];

                while (rs.next()) {
                    int i = rs.getRow() - 1;
                    for (int j = 0; j < columns; j++) {
                        respuesta[i][j] = rs.getObject(j + 1);
                    }
                }
                // l.Guardar_logs_en_la_BD(Sentencia,FuncionesGenerales.Fecha()+" "+FuncionesGenerales.Hora()
                //       , ""+sesion, "SELECT");
                rs.close();
                select.close();

                conexion.close();

            } catch (SQLException e) {
                l.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                        + e.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
                // System.out.println(e.getMessage());
                return new Object[0][0];
            }
        } catch (ClassNotFoundException e) {
            l.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                    + e.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
            return new Object[0][0];
        }

        return respuesta;

    }

    /**
     * Este metodo se encarga de borrar una cuenta contable y todo lo que se
     * relacina a esta.
     *
     * @param sesion
     * @return 1 si todas las relaciones con cuentas con tables son elimadas con
     * éxito, 2 si almenos una no lo es. 3 si no se puede borrar por existir
     * dependencia con otra tabla, o con la misma.
     *
     * public static int DesactivarCuentaContable(int cuenta){ String
     * sentencia="select Count(*) from cuentascontables where " +
     * "subcuentade="+cuenta; int count=Integer.parseInt(Select(sentencia, 1,
     * 1)[0][0].toString()); if(count!=0){ return 3; }else{ sentencia="update
     * cuentascontables where idcuentascontables="+cuenta;
     * if(insert(sentencia)){ return 1; }else { return 2; } }
     *
     * }
     **
     * Este metodo se encarga de borrar una cuenta contable y todo lo que se
     * relacina a esta.
     * @param cuenta; el id de la cuenta contable que se desea borrar.
     * @return true si se eliminan todas las cuentas contables que tienen
     * relacion con el id de la cuenta contable cuenta
     *
     * public static int DesactivarCuentaContableFull(int cuenta){ String
     * sentencia="select Count(*) from cuentascontables where " +
     * "subcuentade="+cuenta; int count=Integer.parseInt(Select(sentencia, 1,
     * 1)[0][0].toString()); if(count!=0){ return 3; }else{ sentencia="delete
     * from cuentas contables where idcuentascontables="+cuenta;
     * if(insert(sentencia)){ return 1; }else { return 2; } }
     *
     * }
     */
    public static String usuario(long sesion) {
        String respuesta;
        String sentencia = "select  f_usuario_from_sesion(" + sesion + ")";
        Logs l = new Logs();
        respuesta = OperacionesBD.Select(sentencia, l, sesion)[0][0].toString();
        l.dispose();
        return respuesta;
    }

    public static void main(String[] args) {
//         / concat(FacturadaEn,'-',idfacturas) 'Factura No.'
        String SQL[] = new String[2];
        SQL[0]
                = "INSERT INTO cencarci1.facturas_proveedores(empresa , factura_proveedores )" + "\r\n"
                + "VALUES (1, 0)";
        SQL[1]
                = "INSERT" + "\r\n"
                + "  INTO cencarci1.componentes_facturas_proveedores(empresa," + "\r\n"
                + "                                                  factura_proveedores," + "\r\n"
                + "                                                  factura_proveedores_linea," + "\r\n"
                + "                                                  cantidad," + "\r\n"
                + "                                                  precio_unitario," + "\r\n"
                + "                                                  monto_bruto," + "\r\n"
                + "                                                  monto_impuesto," + "\r\n"
                + "                                                  monto_descuento," + "\r\n"
                + "                                                  monto_neto," + "\r\n"
                + "                                                  producto)" + "\r\n"
                + "VALUES (1," + "\r\n"
                + "        1," + "\r\n"
                + "        1," + "\r\n"
                + "        0," + "\r\n"
                + "        0,0,0,0,0,1);";

        boolean l = insert_update_con_transaccion(1, new Logs(), SQL, null, null);
        System.out.println(l);

    }

    /**
     * Este metodo busca en la bd si el usuario tiene permisos de programador.
     *
     * @param mylogs
     * @param mysesion
     * @param empleado the value of usuario
     * @return the boolean
     */
    public static boolean ModoDebug(Logs mylogs, long mysesion, String empleado) {
        String SQL
                = "SELECT modo_debug" + "\r\n"
                + "  FROM cencarci1.empleados" + "\r\n"
                + " WHERE idEmpleados =" + empleado;

        Object data[][] = OperacionesBD.Select(SQL, mylogs, mysesion);

        if (data.length == 0) {
            return false;
        }

        try {
            return (boolean) data[0][0];
        } catch (Exception e) {
            try {
                return data[0][0].toString().equals("1");
            } catch (Exception e1) {
            }
        }

        return false;
    }

    /**
     * Este metodo me permite correr una serie de sentencias sql con
     * transaccion. Ademas me permite si asi se desea un query mas que puede
     * usarse para determinar el id que se ha insertado o actualizo o cualquier
     * otro uso. Si se desea utiliazar la funcionalidad de el query al final de
     * correr todos los queries originales entonces SQLID no puede estar en
     * blanco y mypasar no puede ser null
     *
     * @param mysesion sesion donde se esta llamando este metodo
     * @param mylogs mylogs
     * @param sqls arreglo de string que representan las sentencias qu se desea
     * n correr
     * @param mypasar en caso de que se desee hacer un select justo al finalizar
     * todos los sqls se pasara un PasarUnId hacia donde se va a mandar el
     * resultado del query
     * @param sqlID el query que se desee realizar luego de que todos los
     * queries guardados en sqls se hayan ejecutado
     * @return true si todos los queries en sqls corrieron correctamente false
     * si al menos uno dio error.
     */
    public static boolean insert_update_con_transaccion(
            long mysesion,
            Logs mylogs,
            String sqls[],
            PasarUnId mypasar,
            String sqlID
    ) {
        if (sqls.length == 0) {
            return false;
        }
        Statement update = null;
        Connection conexion = null;

        try {
            Class.forName(DRIVER);

            try {
                conexion
                        = DriverManager.getConnection(Connection, user, password);

                conexion.setAutoCommit(false);
                update = conexion.createStatement();

                for (String SQL : sqls) {
                    update.executeUpdate(SQL);

                }
                /**
                 * El String SQLid no está en blanco ni tampoco lo está mypasar
                 * por lo que se corre el query sqlID y se llama pasarunId de
                 * mypasar con el resultado del query
                 */
                if (!FuncionesGenerales.EstaEnBlanco(sqlID) && mypasar != null) {
                    Object[][] respuesta;

                    PreparedStatement select = conexion.prepareStatement(sqlID);

                    ResultSet rs = select.executeQuery();
                    int filas = 0;
                    try {
                        while (rs.next()) {
                            filas++;

                        }
                    } catch (Exception e) {

                    }
                    while (rs.previous()) {

                    }
                    int columns = 0;
                    try {
                        rs.next();
                        //  System.out.println(rs.getRow());
                        while (columns != -1) {
                            Object temp = rs.getObject(columns + 1);
                            columns++;
                        }
                    } catch (Exception force) {
                        rs.previous();
                    }
                    respuesta = new Object[filas][columns];

                    while (rs.next()) {
                        int i = rs.getRow() - 1;
                        for (int j = 0; j < columns; j++) {
                            respuesta[i][j] = rs.getObject(j + 1);
                        }
                    }
                    // l.Guardar_logs_en_la_BD(Sentencia,FuncionesGenerales.Fecha()+" "+FuncionesGenerales.Hora()
                    //       , ""+sesion, "SELECT");
                    rs.close();
                    select.close();

                    mypasar.YaTengoId(respuesta.length > 0 && respuesta[0][0] != null ? 1 : -1, respuesta[0][0] != null ? respuesta[0][0].toString() : null);
                }
                update.close();
                conexion.commit();
                conexion.close();
                //l.Guardar_logs_en_la_BD(Sentencia,FuncionesGenerales.Fecha()+" "+FuncionesGenerales.Hora()                        , ""+mysesion, "INSERT");
                return true;

            } catch (SQLException e) {
                if (!e.getMessage().startsWith("Data truncation: Incorrect datetime value:")) {
                    try {
                        mylogs.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                                + e.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
                        update.close();
                        //System.out.println(e.getMessage());

                        conexion.rollback();
                        conexion.close();
                        return false;
                    } catch (SQLException ex) {
                        mylogs.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                                + ex.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
                    }
                }
            }
        } catch (ClassNotFoundException e) {
            try {
                mylogs.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                        + e.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
                //update.close();
                conexion.rollback();
                conexion.close();

                return false;
            } catch (SQLException ex) {
                mylogs.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                        + e.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
            }
            mylogs.talogs.append("\nError ocurrido en OperacionesBD" + " \n"
                    + e.getMessage() + "\n " + FuncionesGenerales.Fecha() + " " + FuncionesGenerales.Hora());
            return false;
        }

        return false;
    }

    /**
     * Este metodo es practicamente los mismo que insert_update_con_transaccion
     * cuando se le pasa un arreglo de Strings. Solo que a este se le pasa un
     * arrayList Me da la ventaja comparado con el otro que si no se la cantidad
     * de queries que voy a ejecutar simplemente lo pongo en un arrayList y
     * listo me despreocupo de la cantidad. Al final de cuenta este metodo
     * simplemente convierte el ArrayList a un Arreglo de String y ejecuta el
     * metodo orignal.
     *
     * @param mysesion la sesion
     * @param mylogs el Log
     * @param queries los queries que se desean ejecutar
     * @param DB
     * @return true si todos los queries se correieron correctamente, false si
     * almenos uno dio error
     */
    public static boolean insert_update_con_transaccion(long mysesion, Logs mylogs, ArrayList queries, boolean DB) {

        String sqls[] = FuncionesGenerales.Convierte_arreglo_objeto_a_String(FuncionesGenerales.Convertir_de_array_list_a_arreglo_objetos(mylogs, mysesion, queries));
        if (DB) {
            for (String temp : sqls) {
                mylogs.talogs.append(temp + "\n");
            }
        }
        return insert_update_con_transaccion(mysesion, mylogs, sqls, null, null);
    }

    /**
     * Este metodo es practicamente los mismo que insert_update_con_transaccion
     * cuando se le pasa un arreglo de Strings. Solo que a este se le pasa un
     * arrayList Me da la ventaja comparado con el otro que si no se la cantidad
     * de queries que voy a ejecutar simplemente lo pongo en un arrayList y
     * listo me despreocupo de la cantidad. Al final de cuenta este metodo
     * simplemente convierte el ArrayList a un Arreglo de String y ejecuta el
     * metodo orignal.
     *
     * Ademas me permite si asi se desea un query mas que puede usarse para
     * determinar el id que se ha insertado o actualizo o cualquier otro uso. Si
     * se desea utiliazar la funcionalidad de el query al final de correr todos
     * los queries originales entonces SQLID no puede estar en blanco y mypasar
     * no puede ser null
     *
     * @param mysesion la sesion
     * @param mylogs el Log
     * @param queries los queries que se desean ejecutar
     * @param DB si se desea imprimir los queries que se estan ejecutando o no.
     * @param mypasar en caso de que se desee hacer un select justo al finalizar
     * todos los sqls se pasara un PasarUnId hacia donde se va a mandar el
     * resultado del query
     * @param sqlID el query que se desee realizar luego de que todos los
     * queries guardados en sqls se hayan ejecutado
     * @return true si todos los queries se correieron correctamente, false si
     * almenos uno dio error
     */
    public static boolean insert_update_con_transaccion(long mysesion, Logs mylogs, ArrayList queries, boolean DB, PasarUnId mypasar, String sqlID) {

        String sqls[] = FuncionesGenerales.Convierte_arreglo_objeto_a_String(FuncionesGenerales.Convertir_de_array_list_a_arreglo_objetos(mylogs, mysesion, queries));
        if (DB) {
            for (String temp : sqls) {
                mylogs.talogs.append(temp + "\n");
            }
        }
        return insert_update_con_transaccion(mysesion, mylogs, sqls, mypasar, sqlID);
    }
}
