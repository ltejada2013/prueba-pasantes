cd C:\Dropbox\Kyrios\cencarci1\src\MyClasses\resources\images\3D\64x64
@echo off
pushd "C:\Dropbox\Kyrios\cencarci1\src\MyClasses\resources\images\3D\64x64" || exit /b
for /f "eol=: delims=" %%F in ('dir /b /a-d *.png') do (
  for /f "tokens=1* eol=_ delims=_" %%A in ("%%F") do   ren "%%F" "3D_64x64_%%F"
)
popd