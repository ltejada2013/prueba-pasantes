cd C:\Dropbox\Kyrios\cencarci1\src\MyClasses\resources\images\3D\48x48
@echo off
pushd "C:\Dropbox\Kyrios\cencarci1\src\MyClasses\resources\images\3D\48x48" || exit /b
for /f "eol=: delims=" %%F in ('dir /b /a-d *.png') do (
  for /f "tokens=1* eol=_ delims=_" %%A in ("%%F") do   ren "%%F" "3D_48x48_%%F"
)
popd