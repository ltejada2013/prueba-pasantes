cd C:\Dropbox\Kyrios\cencarci1\src\MyClasses\resources\images\3D\32x32
@echo off
pushd "C:\Dropbox\Kyrios\cencarci1\src\MyClasses\resources\images\3D\32x32" || exit /b
for /f "eol=: delims=" %%F in ('dir /b /a-d *.png') do (
  for /f "tokens=1* eol=_ delims=_" %%A in ("%%F") do   ren "%%F" "3D_32x32_%%F"
)
popd